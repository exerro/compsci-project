
local class = require "class"
local powerup = require "powerups"
local controller = require "controller"
local Entity = require "physics.Entity"
local Vec2 = require "physics.Vec2"
local Body = require "physics.Body"
local CircleShape = require "physics.CircleShape"
local FoodSource = require "FoodSource"
local Powerup = require "Powerup"
local Projectile = require "Projectile"

require "globals"

powerup.init()

local speed_boost_image = love.graphics.newImage "res/img/powerup_speed_boost.png"
local size_boost_image = love.graphics.newImage "res/img/powerup_size_boost.png"
local invisible_image = love.graphics.newImage "res/img/powerup_invisible.png"
local bullet_image = love.graphics.newImage "res/img/powerup_bullet.png"

local CELL_BORDER_SIZE = GLOBAL_CONFIG:read "cell.BORDER_SIZE"
local PHASE_START = 4
local PHASE_T = 3

local Cell = class( Entity )

local function draw_powerup_icon( img, x, y, size, rx, ry, bcol, col, invis, timeout )
	local trans = invis and 32 or 255

	if timeout < PHASE_START then
		if invis then
			trans = 32 * math.cos( (PHASE_START - timeout) * PHASE_T / PHASE_START * 2 * math.pi )
		else
			trans = 128 + 127 * math.cos( (PHASE_START - timeout) * PHASE_T / PHASE_START * 2 * math.pi )
		end
	end

	if invis then
		love.graphics.setColor( col[1], col[2], col[3], trans )
		love.graphics.circle( "fill", x + rx * size, y + ry * size, size / 2 )
	else
		love.graphics.setColor( bcol[1], bcol[2], bcol[3], trans )
		love.graphics.circle( "fill", x + rx * size, y + ry * size, size / 2 )
		love.graphics.setColor( col[1], col[2], col[3], trans )
		love.graphics.circle( "fill", x + rx * size, y + ry * size, size / 2 - 3 )
	end

	love.graphics.setColor( 255 - col[1], 255 - col[2], 255 - col[3], trans )
	love.graphics.draw( img, x + (rx - 1/2) * size, y + (ry - 1/2) * size, 0, size / img:getWidth(), size / img:getHeight() )
end

function Cell:new( x, y, size, colour, colour_border )
	size = math.max( size, CELL_MIN_SIZE )

	self:super( Vec2( x, y ), CircleShape( size ), Body.Dynamic )
	self.powerups = {
		invisible = { active = false, effect = 0, timeout = 0 };
		size_boost = { active = false, effect = 0, timeout = 0 };
		speed_boost = { active = false, effect = 0, timeout = 0 };
		bullet = { active = false, effect = 0, timeout = 0 };
	}
	self.colour = colour
	self.colour_border = colour_border
	self.controller = controller.computer()
end

function Cell:draw()
	if self.powerups.invisible.active then
		love.graphics.setColor( self.colour[1], self.colour[2], self.colour[3], 32 )
		love.graphics.circle( "fill", self.position.x, self.position.y, self.shape.radius )
	else
		love.graphics.setColor( self.colour_border )
		love.graphics.circle( "fill", self.position.x, self.position.y, self.shape.radius )
		love.graphics.setColor( self.colour )
		love.graphics.circle( "fill", self.position.x, self.position.y, self.shape.radius - CELL_BORDER_SIZE )
	end

	love.graphics.setColor( 255, 255, 255 )

	if self.powerups.invisible.active then
		draw_powerup_icon( invisible_image, self.position.x, self.position.y, self.shape.radius, -0.9, -0.9, self.colour_border, self.colour, self.powerups.invisible.active, self.powerups.invisible.timeout )
	end

	if self.powerups.size_boost.active then
		draw_powerup_icon( size_boost_image, self.position.x, self.position.y, self.shape.radius, 0.9, -0.9, self.colour_border, self.colour, self.powerups.invisible.active, self.powerups.size_boost.timeout )
	end

	if self.powerups.speed_boost.active then
		draw_powerup_icon( speed_boost_image, self.position.x, self.position.y, self.shape.radius, 0.9, 0.9, self.colour_border, self.colour, self.powerups.invisible.active, self.powerups.speed_boost.timeout )
	end

	if self.powerups.bullet.active then
		draw_powerup_icon( bullet_image, self.position.x, self.position.y, self.shape.radius, -0.9, 0.9, self.colour_border, self.colour, self.powerups.invisible.active, self.powerups.bullet.timeout )
	end
end

function Cell:update( dt )
	for k, v in pairs( self.powerups ) do
		if v.active then
			v.timeout = v.timeout - dt

			if v.timeout <= 0 then
				self:remove_powerup( k )
			end
		end
	end
end

function Cell:is_visible()
	local vx1, vy1, vx2, vy2 = self.world.viewport:get_bounds()
	local bx1, by1, bx2, by2 = self.shape:get_bounds()
	return self.position.x + bx2 >= vx1
	   and self.position.y + by2 >= vy1
   	   and self.position.x + bx1 <= vx2
	   and self.position.y + by1 <= vy2
end

function Cell:add_powerup( name, duration, effect )
	if name == "size_boost" then
		if self.powerups.size_boost.active then
			self.powerups.size_boost.timeout = self.powerups.size_boost.timeout + duration

			if effect > self.powerups.size_boost.effect then
				self.shape.radius = self.shape.radius - self.powerups.size_boost.effect + effect
				self.powerups.size_boost.effect = effect
			end
		else
			self.powerups.size_boost.active = true
			self.powerups.size_boost.effect = effect
			self.powerups.size_boost.timeout = duration
			self.shape.radius = self.shape.radius + effect
		end
	else
		if self.powerups[name].active then
			self.powerups[name].timeout = self.powerups[name].timeout + duration
			self.powerups[name].effect = math.max( self.powerups[name].effect, effect )
		else
			self.powerups[name].active = true
			self.powerups[name].timeout = duration
			self.powerups[name].effect = effect
		end
	end
end

function Cell:remove_powerup( name )
	self.powerups[name].active = false

	if name == "size_boost" then
		self.shape.radius = self.shape.radius - self.powerups.size_boost.effect
	end
end

function Cell:on_collision( entity )
	if entity then
		if entity.class == FoodSource then
		self.shape.radius = math.sqrt( self.shape.radius ^ 2 + entity.shape.radius ^ 2 )
			entity:remove()
		elseif entity.class == Cell then
			if self.shape.radius > entity.shape.radius + EATING_THRESHOLD and self.world and self.world.eating then
				self.shape.radius = math.sqrt( self.shape.radius ^ 2 + entity.shape.radius ^ 2 )
				entity:remove()
			end
		elseif entity.class == Powerup then
			entity:remove()
			powerup.add_to_cell( self, entity.name, entity.potency )
		elseif entity.class == Projectile then
			self.shape.radius = math.sqrt( self.shape.radius ^ 2 - entity.damage )
			entity.owner.shape.radius = math.sqrt( entity.owner.shape.radius ^ 2 + entity.damage )
			if self.shape.radius ^ 2 < CELL_MIN_SIZE then
				self:remove()
			end
		end
	end
end

function Cell:filter( entity )
	return entity.class == Cell or entity.class == Projectile
end

return Cell
