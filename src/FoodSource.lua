
local class = require "class"
local Entity = require "physics.Entity"
local Vec2 = require "physics.Vec2"
local CircleShape = require "physics.CircleShape"
local Body = require "physics.Body"

require "globals"

local FOOD_COLOUR = { 0xf2, 0x68, 0xa1 }

local FoodSource = class( Entity )

function FoodSource:new( x, y, size )
	self:super( Vec2( x, y ), CircleShape( size ), Body.Dynamic )
	-- dynamic because it should be pushed by structures
end

function FoodSource:on_collision( entity )
	-- maybe merge multiple colliding food sources?
end

function FoodSource:filter( entity )
	return entity.type ~= FoodSource -- collides with all non-food
end

function FoodSource:draw()
	love.graphics.setColor( FOOD_COLOUR )
	love.graphics.circle( "fill", self.position.x, self.position.y, self.shape.radius )
end

return FoodSource
