
local sqrt = math.sqrt
local max = math.max

local controller = {}

function controller.player()
	return function( cell, world )
		local tx, ty = world.viewport:world_coords( world.viewport.touch_x, world.viewport.touch_y )
		local dx, dy = tx - cell.position.x, ty - cell.position.y
		local len = max( sqrt( dx * dx + dy * dy ), 1 )
		local scale = world.viewport.touch_magnitude * 50

		return dx / len, dy / len, scale
	end
end

function controller.computer()
	local a = math.random() * 2 * math.pi
	local dx, dy = math.sin( a ), math.cos( a )

	return function( cell, world )
		return dx, dy, 40
	end
end

return controller
