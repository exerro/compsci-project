
local viewport_methods = {}
local viewport_mt = { __index = viewport_methods }

function viewport_methods:new( w, h )
	self.x = 0 -- offset position of the viewport in the world (centre screen)
	self.y = 0
	self.width, self.height = love.window.getMode()
	self.zoom = 1 -- larger value means more zoomed in
	self.target_zoom = 1
	self.touch_x = 0 -- active touch position
	self.touch_y = 0
	self.touch_magnitude = 1
end

function viewport_methods:world_coords( x, y )
	return x / self.zoom + self.x, y / self.zoom + self.y
end

function viewport_methods:screen_coords( x, y )
	return (x - self.x) * self.zoom, (y - self.y) * self.zoom
end

function viewport_methods:apply_love2d_transformation()
	love.graphics.push()
	love.graphics.translate( self.width / 2, self.height / 2 )
	love.graphics.scale( self.zoom )
	love.graphics.translate( -self.x, -self.y )

	if self.zoom < self.target_zoom then
		self.zoom = self.zoom * math.min( 1.005, self.target_zoom / self.zoom )
	elseif self.zoom > self.target_zoom then
		self.zoom = self.zoom / math.min( 1.005, self.zoom / self.target_zoom )
	end

	love.graphics.setColor( 255, 0, 0 )
	love.graphics.circle( "fill", self.touch_x / self.zoom + self.x, self.touch_y / self.zoom + self.y, 5 )
end

function viewport_methods:get_bounds()
	return self.x - self.width / 2 / self.zoom, self.y - self.height / 2 / self.zoom, self.x + self.width / 2 / self.zoom, self.y + self.height / 2 / self.zoom
end

return function( ... )
	local obj = setmetatable( {}, viewport_mt )

	obj:new( ... )

	return obj
end
