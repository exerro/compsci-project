
local names = { "size_boost", "speed_boost", "bullet", "invisible" }
local add_to_cell = {}
local icons = {}
local powerups = {}

function powerups.init()
	for i = 1, #names do
		icons[names[i]] = love.graphics.newImage( "res/img/powerup_" .. names[i] .. ".png" )
	end
end

function powerups.names()
	return names
end

function powerups.random_name()
	local t = powerups.names()
	return t[math.random( 1, #t )]
end

function powerups.add_to_cell( cell, powerup, potency )
	local duration, effect = potency, 1

	if powerup == "speed_boost" then
		effect = 2
	elseif powerup == "size_boost" or powerup == "bullet" then
		duration, effect = 3 / 5 * potency, (2 / 5 * potency) ^ 2
	end

	cell:add_powerup( powerup, duration, effect )
end

function powerups.get_icon( powerup )
	return icons[powerup]
end

return powerups
