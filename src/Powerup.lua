
local class = require "class"
local Entity = require "physics.Entity"
local Vec2 = require "physics.Vec2"
local CircleShape = require "physics.CircleShape"
local Body = require "physics.Body"
local powerups = require "powerups"

require "globals"

local Powerup = class( Entity )

function Powerup:new( x, y, name, potency )
	self:super( Vec2( x, y ), CircleShape( POWERUP_SIZE ), Body.Dynamic )
	self.name = name
	self.potency = potency
end

function Powerup:filter( entity )
	return entity.class == Powerup
end

function Powerup:draw()
	love.graphics.setColor( 255, 255, 255 )
	local img = powerups.get_icon( self.name )

	love.graphics.setColor( 0, 0, 0 )
	love.graphics.circle( "fill", self.position.x, self.position.y, POWERUP_SIZE / 2 )
	love.graphics.setColor( 255, 255, 255 )
	love.graphics.draw( img, self.position.x - POWERUP_SIZE / 2, self.position.y - POWERUP_SIZE / 2, 0, POWERUP_SIZE / img:getWidth(), POWERUP_SIZE / img:getHeight() )
end

return Powerup
