
local coroutines = {}
local list = {}

function coroutines.add( thread )
	if type( thread ) == "function" then
		thread = coroutine.create( thread )
	end

	list[#list + 1] = thread
end

function coroutines.update()
	for i = #list, 1, -1 do
		local ok, err = coroutine.resume( list[i] )
		if not ok then
			error( err, 0 )
		end
		if coroutine.status( list[i] ) == "dead" then
			table.remove( list, i )
		end
	end
end

function coroutines.sleep( n )
	local t = love.timer.getTime() + n
	repeat
		coroutine.yield()
	until love.timer.getTime() >= t
end

return coroutines
