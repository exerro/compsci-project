
local current_state

local state = {}

function state.get()
	return current_state
end

function state.set( s, ... )
	current_state = s
	s:load( ... )
end

return state
