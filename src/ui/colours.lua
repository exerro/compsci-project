
local theme = require "ui.theme"

local function int_to_rgb( i )
	return {
		math.floor( i / 256 ^ 2 );
		math.floor( i / 256 ) % 256;
		i % 256;
	}
end

local colour_list = {
	["Red"] = {
		int_to_rgb( 0xF44336 );
		int_to_rgb( 0xFFEBEE );
		int_to_rgb( 0xFFCDD2 );
		int_to_rgb( 0xEF9A9A );
		int_to_rgb( 0xE57373 );
		int_to_rgb( 0xEF5350 );
		int_to_rgb( 0xF44336 );
		int_to_rgb( 0xE53935 );
		int_to_rgb( 0xD32F2F );
		int_to_rgb( 0xC62828 );
		int_to_rgb( 0xB71C1C );
		int_to_rgb( 0xFF8A80 );
		int_to_rgb( 0xFF5252 );
		int_to_rgb( 0xFF1744 );
		int_to_rgb( 0xD50000 );
	};
	["Pink"] = {
		int_to_rgb( 0xE91E63 );
		int_to_rgb( 0xFCE4EC );
		int_to_rgb( 0xF8BBD0 );
		int_to_rgb( 0xF48FB1 );
		int_to_rgb( 0xF06292 );
		int_to_rgb( 0xEC407A );
		int_to_rgb( 0xE91E63 );
		int_to_rgb( 0xD81B60 );
		int_to_rgb( 0xC2185B );
		int_to_rgb( 0xAD1457 );
		int_to_rgb( 0x880E4F );
		int_to_rgb( 0xFF80AB );
		int_to_rgb( 0xFF4081 );
		int_to_rgb( 0xF50057 );
		int_to_rgb( 0xC51162 );
	};
	["Purple"] = {
		int_to_rgb( 0x9C27B0 );
		int_to_rgb( 0xF3E5F5 );
		int_to_rgb( 0xE1BEE7 );
		int_to_rgb( 0xCE93D8 );
		int_to_rgb( 0xBA68C8 );
		int_to_rgb( 0xAB47BC );
		int_to_rgb( 0x9C27B0 );
		int_to_rgb( 0x8E24AA );
		int_to_rgb( 0x7B1FA2 );
		int_to_rgb( 0x6A1B9A );
		int_to_rgb( 0x4A148C );
		int_to_rgb( 0xEA80FC );
		int_to_rgb( 0xE040FB );
		int_to_rgb( 0xD500F9 );
		int_to_rgb( 0xAA00FF );
	};
	["Deep Purple"] = {
		int_to_rgb( 0x673AB7 );
		int_to_rgb( 0xEDE7F6 );
		int_to_rgb( 0xD1C4E9 );
		int_to_rgb( 0xB39DDB );
		int_to_rgb( 0x9575CD );
		int_to_rgb( 0x7E57C2 );
		int_to_rgb( 0x673AB7 );
		int_to_rgb( 0x5E35B1 );
		int_to_rgb( 0x512DA8 );
		int_to_rgb( 0x4527A0 );
		int_to_rgb( 0x311B92 );
		int_to_rgb( 0xB388FF );
		int_to_rgb( 0x7C4DFF );
		int_to_rgb( 0x651FFF );
		int_to_rgb( 0x6200EA );
	};
	["Indigo"] = {
		int_to_rgb( 0x3F51B5 );
		int_to_rgb( 0xE8EAF6 );
		int_to_rgb( 0xC5CAE9 );
		int_to_rgb( 0x9FA8DA );
		int_to_rgb( 0x7986CB );
		int_to_rgb( 0x5C6BC0 );
		int_to_rgb( 0x3F51B5 );
		int_to_rgb( 0x3949AB );
		int_to_rgb( 0x303F9F );
		int_to_rgb( 0x283593 );
		int_to_rgb( 0x1A237E );
		int_to_rgb( 0x8C9EFF );
		int_to_rgb( 0x536DFE );
		int_to_rgb( 0x3D5AFE );
		int_to_rgb( 0x304FFE );
	};
	["Blue"] = {
		int_to_rgb( 0x2196F3 );
		int_to_rgb( 0xE3F2FD );
		int_to_rgb( 0xBBDEFB );
		int_to_rgb( 0x90CAF9 );
		int_to_rgb( 0x64B5F6 );
		int_to_rgb( 0x42A5F5 );
		int_to_rgb( 0x2196F3 );
		int_to_rgb( 0x1E88E5 );
		int_to_rgb( 0x1976D2 );
		int_to_rgb( 0x1565C0 );
		int_to_rgb( 0x0D47A1 );
		int_to_rgb( 0x82B1FF );
		int_to_rgb( 0x448AFF );
		int_to_rgb( 0x2979FF );
		int_to_rgb( 0x2962FF );
	};
	["Light Blue"] = {
		int_to_rgb( 0x03A9F4 );
		int_to_rgb( 0xE1F5FE );
		int_to_rgb( 0xB3E5FC );
		int_to_rgb( 0x81D4FA );
		int_to_rgb( 0x4FC3F7 );
		int_to_rgb( 0x29B6F6 );
		int_to_rgb( 0x03A9F4 );
		int_to_rgb( 0x039BE5 );
		int_to_rgb( 0x0288D1 );
		int_to_rgb( 0x0277BD );
		int_to_rgb( 0x01579B );
		int_to_rgb( 0x80D8FF );
		int_to_rgb( 0x40C4FF );
		int_to_rgb( 0x00B0FF );
		int_to_rgb( 0x0091EA );
	};
	["Cyan"] = {
		int_to_rgb( 0x00BCD4 );
		int_to_rgb( 0xE0F7FA );
		int_to_rgb( 0xB2EBF2 );
		int_to_rgb( 0x80DEEA );
		int_to_rgb( 0x4DD0E1 );
		int_to_rgb( 0x26C6DA );
		int_to_rgb( 0x00BCD4 );
		int_to_rgb( 0x00ACC1 );
		int_to_rgb( 0x0097A7 );
		int_to_rgb( 0x00838F );
		int_to_rgb( 0x006064 );
		int_to_rgb( 0x84FFFF );
		int_to_rgb( 0x18FFFF );
		int_to_rgb( 0x00E5FF );
		int_to_rgb( 0x00B8D4 );
	};
	["Teal"] = {
		int_to_rgb( 0x009688 );
		int_to_rgb( 0xE0F2F1 );
		int_to_rgb( 0xB2DFDB );
		int_to_rgb( 0x80CBC4 );
		int_to_rgb( 0x4DB6AC );
		int_to_rgb( 0x26A69A );
		int_to_rgb( 0x009688 );
		int_to_rgb( 0x00897B );
		int_to_rgb( 0x00796B );
		int_to_rgb( 0x00695C );
		int_to_rgb( 0x004D40 );
		int_to_rgb( 0xA7FFEB );
		int_to_rgb( 0x64FFDA );
		int_to_rgb( 0x1DE9B6 );
		int_to_rgb( 0x00BFA5 );
	};
	["Green"] = {
		int_to_rgb( 0x4CAF50 );
		int_to_rgb( 0xE8F5E9 );
		int_to_rgb( 0xC8E6C9 );
		int_to_rgb( 0xA5D6A7 );
		int_to_rgb( 0x81C784 );
		int_to_rgb( 0x66BB6A );
		int_to_rgb( 0x4CAF50 );
		int_to_rgb( 0x43A047 );
		int_to_rgb( 0x388E3C );
		int_to_rgb( 0x2E7D32 );
		int_to_rgb( 0x1B5E20 );
		int_to_rgb( 0xB9F6CA );
		int_to_rgb( 0x69F0AE );
		int_to_rgb( 0x00E676 );
		int_to_rgb( 0x00C853 );
	};
	["Light Green"] = {
		int_to_rgb( 0x8BC34A );
		int_to_rgb( 0xF1F8E9 );
		int_to_rgb( 0xDCEDC8 );
		int_to_rgb( 0xC5E1A5 );
		int_to_rgb( 0xAED581 );
		int_to_rgb( 0x9CCC65 );
		int_to_rgb( 0x8BC34A );
		int_to_rgb( 0x7CB342 );
		int_to_rgb( 0x689F38 );
		int_to_rgb( 0x558B2F );
		int_to_rgb( 0x33691E );
		int_to_rgb( 0xCCFF90 );
		int_to_rgb( 0xB2FF59 );
		int_to_rgb( 0x76FF03 );
		int_to_rgb( 0x64DD17 );
	};
	["Lime"] = {
		int_to_rgb( 0xCDDC39 );
		int_to_rgb( 0xF9FBE7 );
		int_to_rgb( 0xF0F4C3 );
		int_to_rgb( 0xE6EE9C );
		int_to_rgb( 0xDCE775 );
		int_to_rgb( 0xD4E157 );
		int_to_rgb( 0xCDDC39 );
		int_to_rgb( 0xC0CA33 );
		int_to_rgb( 0xAFB42B );
		int_to_rgb( 0x9E9D24 );
		int_to_rgb( 0x827717 );
		int_to_rgb( 0xF4FF81 );
		int_to_rgb( 0xEEFF41 );
		int_to_rgb( 0xC6FF00 );
		int_to_rgb( 0xAEEA00 );
	};
	["Yellow"] = {
		int_to_rgb( 0xFFEB3B );
		int_to_rgb( 0xFFFDE7 );
		int_to_rgb( 0xFFF9C4 );
		int_to_rgb( 0xFFF59D );
		int_to_rgb( 0xFFF176 );
		int_to_rgb( 0xFFEE58 );
		int_to_rgb( 0xFFEB3B );
		int_to_rgb( 0xFDD835 );
		int_to_rgb( 0xFBC02D );
		int_to_rgb( 0xF9A825 );
		int_to_rgb( 0xF57F17 );
		int_to_rgb( 0xFFFF8D );
		int_to_rgb( 0xFFFF00 );
		int_to_rgb( 0xFFEA00 );
		int_to_rgb( 0xFFD600 );
	};
	["Amber"] = {
		int_to_rgb( 0xFFC107 );
		int_to_rgb( 0xFFF8E1 );
		int_to_rgb( 0xFFECB3 );
		int_to_rgb( 0xFFE082 );
		int_to_rgb( 0xFFD54F );
		int_to_rgb( 0xFFCA28 );
		int_to_rgb( 0xFFC107 );
		int_to_rgb( 0xFFB300 );
		int_to_rgb( 0xFFA000 );
		int_to_rgb( 0xFF8F00 );
		int_to_rgb( 0xFF6F00 );
		int_to_rgb( 0xFFE57F );
		int_to_rgb( 0xFFD740 );
		int_to_rgb( 0xFFC400 );
		int_to_rgb( 0xFFAB00 );
	};
	["Orange"] = {
		int_to_rgb( 0xFF9800 );
		int_to_rgb( 0xFFF3E0 );
		int_to_rgb( 0xFFE0B2 );
		int_to_rgb( 0xFFCC80 );
		int_to_rgb( 0xFFB74D );
		int_to_rgb( 0xFFA726 );
		int_to_rgb( 0xFF9800 );
		int_to_rgb( 0xFB8C00 );
		int_to_rgb( 0xF57C00 );
		int_to_rgb( 0xEF6C00 );
		int_to_rgb( 0xE65100 );
		int_to_rgb( 0xFFD180 );
		int_to_rgb( 0xFFAB40 );
		int_to_rgb( 0xFF9100 );
		int_to_rgb( 0xFF6D00 );
	};
	["Deep Orange"] = {
		int_to_rgb( 0xFF5722 );
		int_to_rgb( 0xFBE9E7 );
		int_to_rgb( 0xFFCCBC );
		int_to_rgb( 0xFFAB91 );
		int_to_rgb( 0xFF8A65 );
		int_to_rgb( 0xFF7043 );
		int_to_rgb( 0xFF5722 );
		int_to_rgb( 0xF4511E );
		int_to_rgb( 0xE64A19 );
		int_to_rgb( 0xD84315 );
		int_to_rgb( 0xBF360C );
		int_to_rgb( 0xFF9E80 );
		int_to_rgb( 0xFF6E40 );
		int_to_rgb( 0xFF3D00 );
		int_to_rgb( 0xDD2C00 );
	};
	["Brown"] = {
		int_to_rgb( 0x795548 );
		int_to_rgb( 0xEFEBE9 );
		int_to_rgb( 0xD7CCC8 );
		int_to_rgb( 0xBCAAA4 );
		int_to_rgb( 0xA1887F );
		int_to_rgb( 0x8D6E63 );
		int_to_rgb( 0x795548 );
		int_to_rgb( 0x6D4C41 );
		int_to_rgb( 0x5D4037 );
		int_to_rgb( 0x4E342E );
		int_to_rgb( 0x3E2723 );
	};
	["Grey"] = {
		int_to_rgb( 0x9E9E9E );
		int_to_rgb( 0xFAFAFA );
		int_to_rgb( 0xF5F5F5 );
		int_to_rgb( 0xEEEEEE );
		int_to_rgb( 0xE0E0E0 );
		int_to_rgb( 0xBDBDBD );
		int_to_rgb( 0x9E9E9E );
		int_to_rgb( 0x757575 );
		int_to_rgb( 0x616161 );
		int_to_rgb( 0x424242 );
		int_to_rgb( 0x212121 );
	};
	["Blue Grey"] = {
		int_to_rgb( 0x607D8B );
		int_to_rgb( 0xECEFF1 );
		int_to_rgb( 0xCFD8DC );
		int_to_rgb( 0xB0BEC5 );
		int_to_rgb( 0x90A4AE );
		int_to_rgb( 0x78909C );
		int_to_rgb( 0x607D8B );
		int_to_rgb( 0x546E7A );
		int_to_rgb( 0x455A64 );
		int_to_rgb( 0x37474F );
		int_to_rgb( 0x263238 );
	};
}

local colour_keys = {
	"Red";
	"Pink";
	"Purple";
	"Deep Purple";
	"Indigo";
	"Blue";
	"Light Blue";
	"Cyan";
	"Teal";
	"Green";
	"Light Green";
	"Lime";
	"Yellow";
	"Amber";
	"Orange";
	"Deep Orange";
	"Brown";
	"Grey";
	"Blue Grey";
}

local function random_name()
	return colour_keys[math.random( 1, #colour_keys )]
end

local function light_from_name( name )
	return colour_list[name][theme.get() == "light" and 5 or 6]
end

local function dark_from_name( name )
	return colour_list[name][theme.get() == "light" and 8 or 9]
end

local function random()
	return light_from_name( random_name() )
end

local function random_dark()
	return dark_from_name( random_name() )
end

return {
	colours = colour_list;
	random = random;
	random_dark = random_dark;
	random_name = random_name;
	light_from_name = light_from_name;
	dark_from_name = dark_from_name;
}
