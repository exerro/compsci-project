
local state = require "ui.state"
local button = require "ui.button"
local event = require "ui.event"
local util = require "util"
local timer = require "timer"
local coroutines = require "coroutines"
local theme = require "ui.theme"
local powerup = require "powerups"

local touch_stack = {}
local touch_stack_max = 0
local touchrelease = false
local touch_moved = false

local temp_back_button
local world
local helper_font = love.graphics.newFont( 24 )

local sqrt = math.sqrt
local max = math.max

local game_state = {}

function game_state:load( _world )
	temp_back_button = button( 200, 10, 200, 50, "Back" )

	function temp_back_button:onClick()
		state.set( require "ui.states.menu_state" )
	end

	world = _world

	self.particles = {}
	self.helper_message = ""
	self.helper_state = false
	self.helper_transition = false
	self.helper_clock = 0

	coroutines.add( function()
		self.helper_message = "Touch the screen to move towards your finger"
		self.helper_transition = true

		local n = 150

		coroutines.sleep( 0.5 )

		for i = 0, math.pi * 2, math.pi * 2 / n do
			self.particles[#self.particles + 1] = { x = 0.5 + math.sin( i ) / 3, y = 0.5 + math.cos( i ) / 3, clock = 0, lifetime = 0.5, colour = { 70, 140, 210 } }
			coroutines.sleep( 0 )
		end

		coroutines.sleep( 0.5 )
		self.helper_clock = 0
		self.helper_transition = false
	end )

	touch_stack = {}
end

function game_state:draw()
	world:draw()
	temp_back_button:draw()

	local showtext = self.helper_transition
	local w, h = GLOBAL_CONFIG:read "window.WIDTH", GLOBAL_CONFIG:read "window.HEIGHT"

	for i = 1, #self.particles do
		local particle = self.particles[i]
		love.graphics.setColor( particle.colour[1], particle.colour[2], particle.colour[3], 128 + 127 * (1 - particle.clock / particle.lifetime) )
		love.graphics.circle( "fill", particle.x * w, particle.y * h, (1 - particle.clock / particle.lifetime) * 5 + 3 )
	end

	love.graphics.setFont( helper_font )
	love.graphics.setColor( theme.get_text_colour( (showtext and 0 or 255) + self.helper_clock * 255 * (showtext and 1 or -1) ) )
	love.graphics.printf( self.helper_message, GLOBAL_CONFIG:read "window.WIDTH" / 4, 100, GLOBAL_CONFIG:read "window.WIDTH" / 2, "center" )
end

function game_state:update( dt )
	world:update( dt )

	if #world.player_cells == 0 then
		state.set( require "ui.states.dead_state", world:get_player_score() )
	end

	for i = #self.particles, 1, -1 do
		local particle = self.particles[i]
		particle.clock = particle.clock + dt

		if particle.clock >= particle.lifetime then
			table.remove( self.particles, i )
		end
	end

	if self.helper_transition ~= self.helper_state then
		self.helper_clock = self.helper_clock + dt

		if self.helper_clock >= 1 then
			self.helper_state = self.helper_transition
		end
	end
end

function game_state:mousepressed( x, y, button, modifier )
	self:touchpressed( button, x, y, modifier )
end

function game_state:mousereleased( x, y, button, modifier )
	self:touchreleased( button, x, y, modifier )
end

function game_state:mousemoved( x, y, dx, dy, modifier )
	self:touchmoved( 1, x, y, dx, dy, modifier )
end

function game_state:touchpressed( ID, x, y, modifier )
	temp_back_button:handle( event( "touch_down", { ID = ID, x = x - temp_back_button.x, y = y - temp_back_button.y } ) )
	touch_stack[ID] = { x, y }
	touch_stack_max = touch_stack_max + 1
	touchrelease = false
	touch_moved = false
end

function game_state:touchreleased( ID, x, y, modifier )
	temp_back_button:handle( event( "touch_up", { ID = ID, x = x - temp_back_button.x, y = y - temp_back_button.y } ) )
	touch_stack[ID] = nil

	print( ID )

	if not touchrelease and not touch_moved then
		if touch_stack_max == 1 then
			for i = 1, #world.player_cells do
				local player_cell = world.player_cells[i]

				if player_cell.powerups.bullet.active then
					local ww, wh = world.viewport.width, world.viewport.height
					local ux, uy = world.viewport:screen_coords( player_cell.position.x, player_cell.position.y )
					local dx = x - ux - ww / 2
					local dy = y - uy - wh / 2
					local l = math.sqrt( dx ^ 2 + dy ^ 2 )

					world:spawn_projectile( player_cell, dx / l, dy / l, player_cell.powerups.bullet.effect )
				end
			end
		elseif touch_stack_max == 2 then
			for i = 1, #world.player_cells do
				powerup.add_to_cell( world.player_cells[i], "speed_boost", 10 )
			end
		elseif touch_stack_max == 3 then
			for i = 1, #world.player_cells do
				powerup.add_to_cell( world.player_cells[i], "invisible", 10 )
			end
		elseif touch_stack_max == 4 then
			for i = 1, #world.player_cells do
				powerup.add_to_cell( world.player_cells[i], "size_boost", 10 )
			end
		elseif touch_stack_max == 5 then
			for i = 1, #world.player_cells do
				powerup.add_to_cell( world.player_cells[i], "bullet", 10 )
			end
		end
	end

	touchrelease = true

	if not next( touch_stack ) then
		touch_stack_max = 0
		touch_release = false
	end
end

function game_state:touchmoved( ID, x, y, dx, dy, modifier )
	temp_back_button:handle( event( "touch_move", { ID = ID, x = x - temp_back_button.x, y = y - temp_back_button.y, dx = dx, dy = dy } ) )
	touch_stack[ID] = { x, y }

	if util.count_keys( touch_stack ) == 2 then
		if dy > 0 then
			world:zoom_in( dy / 50 )
		elseif dy < 0 then
			world:zoom_out( -dy / 50 )
		end
	end

	if dx ^ 2 + dy ^ 2 > 9 then
		touch_moved = true
	end

	if touch_moved then
		local ww, wh = world.viewport.width, world.viewport.height
		local cx, cy = ww / 2, wh / 2
		local dx, dy = x - cx, y - cy
		local distance = sqrt( dx * dx + dy * dy )
		local maxdist = max( world.viewport.width, world.viewport.height ) / 2
		world.viewport.touch_x, world.viewport.touch_y = x - ww / 2, y - wh / 2
		world.viewport.touch_magnitude = .5 + .5 * util.sigmoid( 14 * distance / maxdist - 2 )
	end
end

function game_state:keypressed( key, modifier )
	if key == "up" then
		world:zoom_in()
	elseif key == "down" then
		world:zoom_out()
	end
end

function game_state:keyreleased( key, modifier )

end

return game_state
