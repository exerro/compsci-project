
local button = require "ui.button"
local label = require "ui.label"
local event = require "ui.event"
local state = require "ui.state"
local theme = require "ui.theme"
local colours = require "ui.colours"
local Cell = require "Cell"
local GameWorld = require "GameWorld"

local font = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 24 )
local font_large = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 64 )
local font_small = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 20 )
local button_image = love.graphics.newImage( "res/menu_button.png" )
local elements = {}
local size_names = { [-1] = "debug small", [0] = "small", [1] = "medium", [2] = "large" }

local size_selected = 0

local function pass_event_to_elements( event, is_mouse_event )
	for i = #elements, 1, -1 do
		elements[i]:handle( is_mouse_event
			and event:child { x = event.parameters.x - elements[i].x, y = event.parameters.y - elements[i].y }
			or event )
	end
end

local new_state = {}

function new_state:load()
	local window_width, window_height = love.window.getMode()

	size_selected = GLOBAL_CONFIG:read "map_size"

	elements[1] = button( window_width * 2/3 - 100, window_height - 110, 200, 60, "Create" )
	elements[2] = button( window_width * 1/3 - 100, window_height - 110, 200, 60, "Cancel" )
	elements[3] = button( window_width / 2 - 150, window_height / 2 - 100, 40, 40, "" )
	elements[4] = button( window_width / 2 + 110, window_height / 2 - 100, 40, 40, "" )
	elements[5] =  label( window_width / 2 - 100, window_height / 2 - 140, 200, 50, "map size" )
	elements[6] =  label( window_width / 2 - 100, window_height / 2 - 100, 200, 50, size_names[size_selected] )

	for i = 1, 2 do
		elements[i].font = font
		elements[i].image = button_image
		elements[i].colour = { 255, 255, 255 }
	end

	elements[5].font = font_small
	elements[6].font = font_small

	elements[5].text_colour = theme.get_text_colour()
	elements[6].text_colour = theme.get_text_colour()

	elements[3].image = love.graphics.newImage "res/arrow_left.png"
	elements[4].image = love.graphics.newImage "res/arrow_right.png"

	elements[1].key_activator = "return"
	elements[2].key_activator = "backspace"

	elements[1].onClick = function( self )
		local game_state = require "ui.states.game_state"
		local view_width, view_height = GLOBAL_CONFIG:read "window.WIDTH", GLOBAL_CONFIG:read "window.HEIGHT"
		local colour_name = colours.random_name()
		local player = Cell( 0, 0, INITIAL_PLAYER_SIZE, colours.light_from_name( colour_name ), colours.dark_from_name( colour_name ) )
		local world = GameWorld( size_selected )

		world:add_entity( player )
		world:register_player_cell( player )

		for x = 1, world.chunknh, 2 do
			for y = 1, world.chunknv, 2 do
				world:spawn_food_in_area( 2, world:get_chunk_bounds( x - 1, y - 1, x, y ) )
			end
		end

		for x = 1, world.chunknh, 3 do
			for y = 1, world.chunknv, 3 do
				world:spawn_cells_in_area( 1, world:get_chunk_bounds( x - 1, y - 1, x + 1, y + 1 ) )
			end
		end

		for x = 1, world.chunknh, 3 do
			for y = 1, world.chunknv, 3 do
				world:spawn_powerups_in_area( 1, world:get_chunk_bounds( x - 1, y - 1, x + 1, y + 1 ) )
			end
		end

		state.set( game_state, world )
	end

	elements[2].onClick = function( self )
		state.set( require "ui.states.menu_state" )
	end

	elements[3].onClick = function( self )
		size_selected = size_selected - 1
		elements[6].text = size_names[size_selected]
		elements[3].visible = size_names[size_selected - 1] ~= nil
		elements[4].visible = true
	end

	elements[4].onClick = function( self )
		size_selected = size_selected + 1
		elements[6].text = size_names[size_selected]
		elements[3].visible = true
		elements[4].visible = size_names[size_selected + 1] ~= nil
	end

	elements[3].visible = size_selected > MAP_SIZE_DEBUG
	elements[4].visible = size_selected < MAP_SIZE_LARGE
end

function new_state:update( dt )
	for i = 1, #elements do
		elements[i]:update( dt )
	end
end

function new_state:mousepressed( x, y, button, modifier )
	pass_event_to_elements( event( "mouse_down", { x = x, y = y, button = button } ), true )
end

function new_state:mousereleased( x, y, button, modifier )
	pass_event_to_elements( event( "mouse_up", { x = x, y = y, button = button } ), true )
end

function new_state:touchpressed( ID, x, y, button, modifier )
	pass_event_to_elements( event( "touch_down", { ID = ID, x = x, y = y, button = button } ), true )
end

function new_state:touchreleased( ID, x, y, button, modifier )
	pass_event_to_elements( event( "touch_up", { ID = ID, x = x, y = y, button = button } ), true )
end

function new_state:keypressed( key, modifier )
	pass_event_to_elements( event( "key_down", { name = modifier .. key, raw = key, modifier = modifier } ), false )
end

function new_state:keyreleased( key, modifier )
	pass_event_to_elements( event( "key_up", { name = modifier .. key, raw = key, modifier = modifier } ), false )
end

function new_state:draw()
	for i = 1, #elements do
		elements[i]:draw()
	end

	love.graphics.setFont( font_large )
	love.graphics.setColor( theme.get_text_colour() )
	love.graphics.print( "Create game", love.window.getMode() / 2 - font_large:getWidth "Create game" / 2, 10 )
end

return new_state
