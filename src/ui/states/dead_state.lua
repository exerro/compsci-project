
local buttonobj = require "ui.button"
local label = require "ui.label"
local event = require "ui.event"
local state = require "ui.state"
local theme = require "ui.theme"

local font = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 24 )
local font_large = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 64 )
local font_small = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 20 )
local button_image = love.graphics.newImage( "res/menu_button.png" )
local button, score

local function pass_event_to_element( event, is_mouse_event )
	button:handle( is_mouse_event
			and event:child { x = event.parameters.x - button.x, y = event.parameters.y - button.y }
			or event )
end

local dead_state = {}

function dead_state:load( _score )
	local window_width, window_height = love.window.getMode()

	score = "Score: " .. tostring( _score )

	button = buttonobj( window_width * 1/2 - 200, window_height - 110, 400, 60, "Back to menu" )
	button.font = font
	button.image = button_image
	button.colour = { 255, 255, 255 }

	button.onClick = function()
		state.set( require "ui.states.menu_state" )
	end
end

function dead_state:update( dt )
	button:update( dt )
end

function dead_state:mousepressed( x, y, button, modifier )
	pass_event_to_element( event( "mouse_down", { x = x, y = y, button = button } ), true )
end

function dead_state:mousereleased( x, y, button, modifier )
	pass_event_to_element( event( "mouse_up", { x = x, y = y, button = button } ), true )
end

function dead_state:touchpressed( ID, x, y, button, modifier )
	pass_event_to_element( event( "touch_down", { ID = ID, x = x, y = y, button = button } ), true )
end

function dead_state:touchreleased( ID, x, y, button, modifier )
	pass_event_to_element( event( "touch_up", { ID = ID, x = x, y = y, button = button } ), true )
end

function dead_state:keypressed( key, modifier )
	pass_event_to_element( event( "key_down", { name = modifier .. key, raw = key, modifier = modifier } ), false )
end

function dead_state:keyreleased( key, modifier )
	pass_event_to_element( event( "key_up", { name = modifier .. key, raw = key, modifier = modifier } ), false )
end

function dead_state:draw()
	button:draw()
	love.graphics.setFont( font_large )
	love.graphics.setColor( theme.get_text_colour() )
	love.graphics.print( "You died", love.window.getMode() / 2 - font_large:getWidth "You died" / 2, 10 )
	love.graphics.print( score, love.window.getMode() / 2 - font_large:getWidth( score ) / 2, 50 + font_large:getHeight() )
end

return dead_state
