
local button = require "ui.button"
local event = require "ui.event"
local colours = require "ui.colours"
local state = require "ui.state"
local cell = require "cell"
local theme = require "ui.theme"
local GameWorld = require "GameWorld"

local menu_font = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 24 )
local menu_font_large = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 120 )
local button_image = love.graphics.newImage( "res/menu_button.png" )
local title_shader_code = love.filesystem.read "res/shaders/wavy.glsl"
local title_shader
local time = 0
local buttons = {}
local menu_title_image
local world

local function pass_event_to_buttons( event, is_mouse_event )
	for i = #buttons, 1, -1 do
		buttons[i]:handle( is_mouse_event
			and event:child { x = event.parameters.x - buttons[i].x, y = event.parameters.y - buttons[i].y }
			or event )
	end
end

local menu_state = {}

function menu_state:load()
	local window_width, window_height = love.window.getMode()
	local canvas = love.graphics.newCanvas( menu_font_large:getWidth "Jelloid" + 20, menu_font_large:getHeight() )

	world = GameWorld( MAP_SIZE_SMALL )
	world.eating = false
	world.spawn_artifacts = false
	world.non_convergent_collisions = true
	buttons = {}

	love.graphics.setCanvas( canvas )
	love.graphics.setFont( menu_font_large )
	love.graphics.setColor( 255, 255, 255 )
	love.graphics.print( "Jelloid", 10, -10 )
	love.graphics.setCanvas()

	menu_title_image = love.graphics.newImage( canvas:newImageData() )

	title_shader = love.graphics.newShader( title_shader_code )

	buttons[1] = button( window_width - 350, 200, 300, 80, "Play" )
	buttons[2] = button( window_width - 350, 310, 300, 80, "Options" )
	buttons[3] = button( window_width - 350, 420, 300, 80, "Exit" )

	for i = 1, 3 do
		buttons[i].font = menu_font
		buttons[i].image = button_image
		buttons[i].colour = { 255, 255, 255 }
	end

	buttons[3].key_activator = "ctrl-w"

	buttons[1].onClick = function( self )
		state.set( require "ui.states.new_state" )
	end

	buttons[2].onClick = function( self )
		state.set( require "ui.states.options_state" )
	end

	buttons[3].onClick = function( self )
		love.event.quit()
	end

	local x, y = world:get_centre()

	world:spawn_cells_in_area( 10, x - world.viewport.width / 2, y - world.viewport.height / 2, x + world.viewport.width / 2, y + world.viewport.height / 2 )
	world.viewport.x = x
	world.viewport.y = y
end

function menu_state:update( dt )
	local x, y = world:get_centre()

	world:update( dt )

	for i = 1, #buttons do
		buttons[i]:update( dt )
	end

	for i = #world.entities, 1, -1 do
		if not world.entities[i]:is_visible( world.viewport ) then
			world.entities[i]:remove()
			world:spawn_cells_in_area( 1, x - world.viewport.width / 2, y - world.viewport.height / 2, x + world.viewport.width / 2, y + world.viewport.height / 2 )
		end
	end

	time = time + dt
	title_shader:send( "time", time )
end

function menu_state:mousepressed( x, y, button, modifier )
	pass_event_to_buttons( event( "mouse_down", { x = x, y = y, button = button } ), true )
end

function menu_state:mousereleased( x, y, button, modifier )
	pass_event_to_buttons( event( "mouse_up", { x = x, y = y, button = button } ), true )
end

function menu_state:touchpressed( ID, x, y, button, modifier )
	pass_event_to_buttons( event( "touch_down", { ID = ID, x = x, y = y, button = button } ), true )
end

function menu_state:touchreleased( ID, x, y, button, modifier )
	pass_event_to_buttons( event( "touch_up", { ID = ID, x = x, y = y, button = button } ), true )
end

function menu_state:keypressed( key, modifier )
	pass_event_to_buttons( event( "key_down", { name = modifier .. key, raw = key, modifier = modifier } ), false )
end

function menu_state:keyreleased( key, modifier )
	pass_event_to_buttons( event( "key_up", { name = modifier .. key, raw = key, modifier = modifier } ), false )
end

function menu_state:draw()
	world:draw()

	for i = 1, #buttons do
		buttons[i]:draw()
	end

	love.graphics.setShader( title_shader )
	love.graphics.setColor( theme.get_text_colour() )
	love.graphics.draw( menu_title_image, 30, 0 )
	love.graphics.setShader()
end

return menu_state
