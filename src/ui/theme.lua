
local current = GLOBAL_CONFIG:read "theme"

do
	if current ~= "light" and current ~= "dark" then
		current = "light"
	end
end

local theme = {}

function theme.get_background_colour()
	if current == "light" then
		return { 240, 240, 240 }
	else
		return { 40, 40, 40 }
	end
end

function theme.get_text_colour( t )
	if current == "light" then
		return { 0, 0, 0, t or 200 }
	else
		return { 255, 255, 255, t or 200 }
	end
end

function theme.get_grid_colour( t )
	if current == "light" then
		return { 0, 0, 0, t or 0.2 }
	else
		return { 1, 1, 1, t or 0.2 }
	end
end

function theme.set( theme )
	current = (theme == "light" or theme == "dark") and theme or current
	GLOBAL_CONFIG:write( "theme", current )
end

function theme.get()
	return current
end

return theme
