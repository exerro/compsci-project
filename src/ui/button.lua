
local util = require "util"
local class = require "class"
local label = require "ui.label"

local button = class( label )

function button:new( x, y, w, h, t )
	self:super( x, y, w, h, t )

	self.visible = true
	self.down, self.down_x, self.down_y = false, 0, 0
	self.colour = { 0, 128, 255 }
	self.text_colour = { 255, 255, 255, 200 }
	self.shadow = 0
	self.font = love.graphics.newFont( 28 )
end

function button:draw()
	if not self.visible then return end

	if self.down then
		love.graphics.setColor( self.colour[1] * 0.8, self.colour[2] * 0.8, self.colour[3] * 0.8, self.colour[4] )
	else
		love.graphics.setColor( self.colour )
	end

	if self.image then
		if self.down then
			love.graphics.draw( self.image, self.x, self.y + 3, 0, self.width / self.image:getWidth(), self.height / self.image:getHeight() )
		else
			love.graphics.draw( self.image, self.x, self.y, 0, self.width / self.image:getWidth(), self.height / self.image:getHeight() )
		end
	else
		love.graphics.rectangle( "fill", self.x, self.y, self.width, self.height )
	end

	love.graphics.setFont( self.font )
	love.graphics.setColor( self.text_colour )
	love.graphics.print( self.text, self.x + self.width / 2 - self.font:getWidth( self.text ) / 2, self.y + self.height / 2 - self.font:getHeight() / 2 )
end

function button:handle( event )
	if event.name == "mouse_down" or event.name == "touch_down" then
		if util.test_point_box( event.parameters.x, event.parameters.y, 0, 0, self.width, self.height ) then
			self.down_x = event.parameters.x
			self.down_y = event.parameters.y
			self.down = true

			event:handle( self )
		end

	elseif event.name == "mouse_up" or event.name == "touch_up" then
		if self.down then
			self.down = false

			if math.sqrt( (event.parameters.x - self.down_x) ^ 2 + (event.parameters.y - self.down_y) ^ 2 ) then
				if self.onClick and self.visible then
					self:onClick( event.parameters.button )
				end

				event:handle( self )
			end
		end

	elseif event.name == "key_down" and event.parameters.name == self.key_activator then
		if self.onClick and self.visible then
			self:onClick( event.parameters.name )
		end

		event:handle( self )

	elseif event.name == "key_up" and event.parameters.name == self.key_activator then
		event:handle( self )

	end
end

return button
