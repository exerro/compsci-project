
local config_methods = {}

local function serialize( data )
	if type( data ) == "table" then
		local t = {}
		local s = {}
		local nonintkey = false

		for i = 1, #data do
			s[1] = (s[1] and s[1] .. ", " or "") .. serialize( data[i] ):gsub( "\n", "\n\t" )
			t[i] = true
		end

		for k, v in pairs( data ) do
			if not t[k] then
				s[#s + 1] = (type( k ) == "string" and not k:find "[^%w_]" and k or "[" .. serialize( k ):gsub( "\n", "\n\t" ) .. "]") .. " = " .. serialize( v ):gsub( "\n", "\n\t" )
				nonintkey = true
			end
		end

		if nonintkey then
			return "{\n\t" .. table.concat( s, ";\n\t" ) .. "\n}"
		else
			return "{ " .. s[1] .. " }"
		end
	elseif type( data ) == "string" then
		return "'" .. data:gsub( "\\", "\\\\" ):gsub( "'", "\\'" ) .. "'"
	else
		return tostring( data )
	end
end

local function unserialize( data )
	local f, err = loadstring( "return " .. data, "config content" )

	if f then
		f, err = pcall( setfenv( f, {} ) )
	end

	return f and err, err
end

local function open_config( path )
	local config = setmetatable( { path = path, data = {}, autosaving = false, changed = false }, { __index = config_methods } )

	if love.filesystem.isFile( path ) then
		local data = unserialize( love.filesystem.read( path ) )

		if type( data ) ~= "table" then
			return nil, "corrupt config file"
		end

		config.data = data
	elseif love.filesystem.exists( path ) then
		return nil, "failed to open config file"
	end

	return config
end

function config_methods:changed_since( t )
	return t < self.changed
end

function config_methods:read( index )
	local v = self.data

	for seg in index:gmatch "[^.]+" do
		if type( v ) ~= "table" then
			return nil
		end

		v = v[seg]
	end

	return v
end

function config_methods:exists( index )
	return self:read( index ) ~= nil
end

function config_methods:write( index, value )
	local v = self.data
	local segments = {}

	for seg in index:gmatch "[^.]+" do
		segments[#segments + 1] = seg
	end

	for i = 1, #segments - 1 do
		if type( v[segments[i]] ) ~= "table" then
			v[segments[i]] = {}
		end
		v = v[segments[i]]
	end

	v[segments[#segments]] = value
	self.changed = os.clock()

	if self.autosaving then
		self:save()
	end

	return self
end

function config_methods:clear( index )
	return self:write( index, nil )
end

function config_methods:autosave()
	self.autosaving = true

	if self.changed then
		self:save()
	end

	return self
end

function config_methods:save()
	if not self.changed then
		return true
	end

	return love.filesystem.write( self.path, serialize( self.data ) )
end

function config_methods:close()
	if self:save() then
		setmetatable( self, {} )

		return true
	end

	return false
end

return { open = open_config }
