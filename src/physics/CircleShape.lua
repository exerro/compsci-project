
local class = require "class"
local Shape = require "physics.Shape"

local pi = math.pi

local CircleShape = class( Shape )

function CircleShape:new( radius )
	self:super()
	self.radius = radius
end

function CircleShape:get_area()
	local r = self.radius
	return pi * r * r
end

function CircleShape:get_bounds()
	local r = self.radius
	return -r, -r, r, r
end

return CircleShape
