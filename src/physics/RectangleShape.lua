
local class = require "class"
local Shape = require "physics.Shape"

local RectangleShape = class( Shape )

function RectangleShape:new( width, height )
	self:super()
	self.width = width
	self.height = height
end

function RectangleShape:get_area()
	return self.width * self.height
end

function RectangleShape:get_bounds()
	return 0, 0, self.width, self.height
end

return RectangleShape
