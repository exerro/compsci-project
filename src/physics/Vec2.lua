
local class = require "class"

local sqrt = math.sqrt
local floor = math.floor

local Vec2 = class()

function Vec2:new( x, y )
	self.x = x or 0
	self.y = y or 0
end

function Vec2:add( vec )
	return Vec2( self.x + vec.x, self.y + vec.y )
end

function Vec2:sub( vec )
	return Vec2( self.x - vec.x, self.y - vec.y )
end

function Vec2:unm()
	return Vec2( -self.x, -self.y )
end

function Vec2:mul( k )
	return Vec2( self.x * k, self.y * k )
end

function Vec2:div( k )
	return Vec2( self.x / k, self.y / k )
end

function Vec2:len()
	local x, y = self.x, self.y
	return sqrt( x * x + y * y )
end

function Vec2:floored()
	return Vec2( floor( self.x ), floor( self.y ) )
end

function Vec2:dot( vec )
	return self.x * vec.x + self.y * vec.y
end

function Vec2:rot90()
	return Vec2( -self.y, self.x )
end

function Vec2:tostring()
	return "(" .. self.x .. ", " .. self.y .. ")"
end

Vec2.zero = Vec2( 0, 0 )

return Vec2
