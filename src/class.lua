
local blank_function = function() end
local metas = { "add", "sub", "mul", "div", "len", "unm", "eq", "tostring" }

return function( super )
	local mt = { __index = super }
	local class = setmetatable( { meta = mt }, mt )

	if super then
		class.super = super.new
	end

	class.new = blank_function

	function mt:__call( ... )
		local mt = { __index = self }
		local obj = setmetatable( { meta = mt, class = self }, mt )

		for i = 1, #metas do
			mt["__" .. metas[i]] = obj[metas[i]]
		end

		obj:new( ... )

		return obj
	end

	return class
end
