
extern float time;

vec4 effect(vec4 colour, Image texture, vec2 texture_coords, vec2 screen_coords) {
	vec2 mod_coords = vec2( texture_coords.x + sin( 2.f * time + texture_coords.y * 5 ) / 50, texture_coords.y );
	vec4 pixel = Texel( texture, mod_coords );
	return pixel * colour;
}
