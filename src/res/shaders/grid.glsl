
extern vec2 draw_offset;
extern float draw_scale;
extern vec4 grid_colour;

vec4 effect(vec4 colour, Image texture, vec2 image_coords, vec2 screen_coords) {
	vec2 world_coords = (screen_coords + draw_offset) / draw_scale;
	float divisor = 128.0;
	float lt_test = 1 / draw_scale;

	if (mod( floor( world_coords.x ), divisor ) < lt_test || mod( floor( world_coords.y ), divisor ) < lt_test) {
		return grid_colour;
	}
	else {
		return vec4( 0.0, 0.0, 0.0, 0.0 );
	}
}
